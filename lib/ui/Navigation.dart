import 'dart:async';

import 'package:flutter/material.dart';
import 'package:russ_test/ui/avia/AviaScreen.dart';
import 'package:russ_test/ui/feed/FeedScreen.dart';
import 'package:russ_test/utils/Constants.dart';
import 'package:russ_test/utils/CustomNotification.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Navigation extends StatefulWidget {
  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  int _selectedIndex = 0;

  bool _isFeedReady = false;
  bool _isAviaReady = false;

  OverlayEntry _overlayEntry;

  final List<Widget> pages = [];

  Widget _bottomNavigationBar(int selectedIndex) => BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.format_list_bulleted),
            title: Text('Feed'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.airplanemode_active),
            title: Text('Avia'),
          ),
        ],
        currentIndex: selectedIndex,
        selectedItemColor: Colors.red,
        onTap: (int index) => setState(() => _selectedIndex = index),
      );

  OverlayEntry initOverlay() {
    return OverlayEntry(
      builder: (context) => Positioned(
        child: Material(
          child: Container(
            child: SpinKitFoldingCube(
              color: Colors.white,
              size: 75,
            ),
            color: Constants.splash,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
        ),
      ),
    );
  }

  void handleNotification(CustomNotification n) {
    switch (n.s) {
      case WebViewState.LOADING:
        {
          showOverlay();
          break;
        }
      case WebViewState.FEED_LOADED:
        {
          setState(() {
            _isFeedReady = true;
          });
          break;
        }
      case WebViewState.AVIA_LOADED:
        {
          setState(() {
            _isAviaReady = true;
          });
          break;
        }
    }
    if (_isAviaReady && _isFeedReady) {
      _overlayEntry.remove();
    }
  }

  void showOverlay() {
    _overlayEntry = initOverlay();
    Overlay.of(context).insert(_overlayEntry);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: NotificationListener<CustomNotification>(
          child: IndexedStack(
            index: _selectedIndex,
            children: <Widget>[
              FeedScreen(
                isFeedReady: _isFeedReady,
              ),
              AviaScreen()
            ],
          ),
          onNotification: (notification) {
            handleNotification(notification);
            return true;
          },
        ),
      ),
      bottomNavigationBar: _bottomNavigationBar(_selectedIndex),
    );
  }
}
