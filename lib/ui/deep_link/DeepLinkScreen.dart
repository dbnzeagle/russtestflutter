import 'package:flutter/material.dart';
import 'package:russ_test/utils/Constants.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DeepLinkScreen extends StatelessWidget {
  final String url;

  DeepLinkScreen({this.url});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constants.greyColor,
      child: SafeArea(
        bottom: false,
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height - 24,
              child: WebView(
                initialUrl: url,
                javascriptMode: JavascriptMode.unrestricted,
              ),
            ),
            Positioned(
              left: 24,
              top: 24,
              child: GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Container(
                  height: 36,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(45),
                  ),
                  width: 36,
                  child: Icon(Icons.arrow_back_ios),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
