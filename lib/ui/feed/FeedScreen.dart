import 'dart:async';

import 'package:flutter/material.dart';
import 'package:russ_test/ui/deep_link/DeepLinkScreen.dart';
import 'package:russ_test/utils/Constants.dart';
import 'package:russ_test/utils/CustomNotification.dart';
import 'package:webview_flutter/webview_flutter.dart';

class FeedScreen extends StatelessWidget {
  final bool isFeedReady;

  FeedScreen({this.isFeedReady});

  WebViewController webViewController;

  void handleRedirect(String url, BuildContext context) {
    if (url.contains("event")) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (_) => DeepLinkScreen(
            url: url.replaceFirst("russpass://", Constants.feedUrl),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: WebView(
        initialUrl: Constants.feedUrl,
        javascriptMode: JavascriptMode.unrestricted,
        navigationDelegate: (request) {
          if (request.url.contains('event')) {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (_) => DeepLinkScreen(
                  url: request.url.replaceFirst("russpass://", Constants.feedUrl),
                ),
              ),
            );
            return NavigationDecision.prevent;
          }
          return NavigationDecision.navigate;
        },
        onPageStarted: (url) {
          handleRedirect(url, context);
          if (!isFeedReady) {
            CustomNotification(s: WebViewState.LOADING).dispatch(context);
          } else {
            webViewController.reload();
          }
        },
        onWebViewCreated: (v) {
          webViewController = v;
        },
        onPageFinished: (v) {
          CustomNotification(s: WebViewState.FEED_LOADED).dispatch(context);
        },
      ),
    );
  }
}
