
import 'package:flutter/material.dart';
import 'package:russ_test/utils/Constants.dart';
import 'package:russ_test/utils/CustomNotification.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AviaScreen extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Center(
      child: WebView(
        initialUrl: Constants.aviaUrl,
        javascriptMode: JavascriptMode.unrestricted,
        onPageFinished: (webViewController) {
          CustomNotification(s: WebViewState.AVIA_LOADED).dispatch(context);
        },
      ),
    );
  }
}
