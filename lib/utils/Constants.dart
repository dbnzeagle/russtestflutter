import 'dart:ui';

class Constants {
  static const feedUrl = "https://explore.russpass.ru/";
  static const aviaUrl = "https://russpass.ru/avia-tickets";
  static const greyColor = Color.fromARGB(65, 20, 20, 20);
  static const splash = Color(0xff000000);
}
