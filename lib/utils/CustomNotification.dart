import 'package:flutter/widgets.dart';

enum WebViewState {
  LOADING,
  FEED_LOADED,
  AVIA_LOADED,
}

class CustomNotification extends Notification {
  final WebViewState s;

  CustomNotification({this.s});
}
